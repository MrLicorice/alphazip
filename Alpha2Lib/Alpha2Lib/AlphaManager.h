#pragma once
class AlphaManager
{
	typedef struct SVFSEntry {
		int iIndex;
		std::string sPackName;
		std::string sArchivedName;
		std::string sVFSName;
	} TVFSEntry, *PVFSEntry;
	typedef std::map<DWORD, TVFSEntry> TVFSMap;
	struct SCompareStringByLength {
		bool operator()(const std::string& first, const std::string& second) {
			return first.size() < second.size();
		}
	};
	struct SFindFileName : public std::unary_function<std::string, bool>
	{
		explicit SFindFileName(const std::string & basePath) : basePath(basePath) {}
		
		bool operator() (const std::string &arg)
		{
			if (basePath == "."|| basePath == "*") {
				return true;
			}
			if (arg.length() < basePath.length())
				return false;
			return arg.compare(0, basePath.length(), basePath) == 0;
		}
		std::string basePath;
	};
	struct RecursiveDirectoryRange
	{
		typedef std::experimental::filesystem::recursive_directory_iterator iterator;
		RecursiveDirectoryRange(std::experimental::filesystem::path p) : p_(p) {}

		iterator begin() { return std::experimental::filesystem::recursive_directory_iterator(p_); }
		iterator end() { return std::experimental::filesystem::recursive_directory_iterator(); }

		std::experimental::filesystem::path p_;
	};
public:
	AlphaManager();
	~AlphaManager() {};
	//
	void SetPassword(std::string pwd) { sPassword = pwd; }
	void Initialize(std::string packList);
	bool LoadPack(std::string fileName);
	//VFS
	bool Exists(std::string fileName, TVFSMap::iterator* itOut = nullptr);
	bool Get(std::string fileName, std::unique_ptr<char>* outData, int* outSize);
	bool GetVFSList(std::vector<std::string>& outList);
	bool ListDir(std::string path, std::vector<std::string>& outList);
	bool ListDiskDir(std::string baseDir, std::vector<std::string>& outList);
	bool AddFile(std::string filePath, std::string basePath, std::string pack, bool bCreate = false);
	bool AddFiles(std::vector<std::string> filesPath, std::string basePath, std::string pack, bool bCreate = false);
	//meant to be private
	void StrReplace(std::string& source, std::string const& find, std::string const& replace);
private:
	DWORD HashFileName(std::string fileName);
	std::string FileNameToVFSName(std::string fileName);
	

	TVFSMap mFiles;
	std::vector<std::string> vNames;
	bool bAllowOverride;
	std::string sPassword;
	//Utils
	
	inline bool FileExists(const std::string& name) {
		struct stat buffer;
		return (stat(name.c_str(), &buffer) == 0);
	}
};
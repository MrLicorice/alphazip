#include "stdafx.h"
#include <algorithm>
#include <filesystem>
#include <ZipLib/ZipFile.h>
#include <ZipLib/ZipArchive.h>
#include <ZipLib/streams/memstream.h>
#include <ZipLib/methods/DeflateMethod.h>
#include <fstream>
#include <windows.h>
#include "MurmurHash3.h"
#include "AlphaManager.h"

#pragma comment( lib, "ZipLib.lib" )
#pragma comment( lib, "zlib.lib" )
#pragma comment( lib, "lzma.lib" )
#pragma comment( lib, "bzip2.lib" )


AlphaManager::AlphaManager()
{
	srand(time(0));
	bAllowOverride = true;
	sPassword = "";
}

bool AlphaManager::Exists(std::string fileName, TVFSMap::iterator* itOut)
{
	DWORD dwHash = HashFileName(fileName);
	TVFSMap::iterator it = mFiles.find(dwHash);
	if (it == mFiles.end())
		return false;
	if (itOut != nullptr) {
		*itOut = it;
	}
	return true;
}

bool AlphaManager::Get(std::string fileName, std::unique_ptr<char>* outData, int* outSize)
{
	TVFSMap::iterator it;
	if (!Exists(fileName, &it)) {
		//Error: file does not exist
		return false;
	}
	//Placeholder: check if file is cached
	TVFSEntry& entry = it->second;
	ZipArchive::Ptr archive = ZipFile::Open(entry.sPackName);
	ZipArchiveEntry::Ptr aEntry = archive->GetEntry(entry.iIndex);
	
	if (!aEntry) {
		aEntry = archive->GetEntry(entry.sArchivedName);
	}
	if (!aEntry) {
		//Error: failed to get entry for requested file.
		return false;
	}
	if (aEntry->IsPasswordProtected() && sPassword == "") {
		//Error: file is password protected, and no password is set.
		return false;
	}
	else if (aEntry->IsPasswordProtected()) {
		aEntry->SetPassword(sPassword);
	}

	std::istream* dcStream = aEntry->GetDecompressionStream();
	if (!dcStream) {
		//Error: failed to get decompression stream
		return false;
	}
	dcStream->seekg(dcStream->beg);
	 //Warning: could overflow.
	*outSize = aEntry->GetSize();
	*outData = std::make_unique<char>(*outSize);
	dcStream->read(outData->get(), *outSize);
	return true;
}

bool AlphaManager::GetVFSList(std::vector<std::string>& outList)
{
	outList = vNames;
	return outList.size() > 0;
}

bool AlphaManager::ListDir(std::string path, std::vector<std::string>& outList)
{
	//Iterate until we reach end of vector, keeping track of last position
	std::vector<std::string>::iterator it = vNames.begin();//DoRaeMon: Think about using a string reference vector for better performance
	do {
		it = std::find_if(it, vNames.end(), SFindFileName(path));//Assuming strings are ordered and this does a O(n) search
		if (it != vNames.end())
			outList.push_back(*it);
	} while (it != vNames.end());
		
	return outList.size() > 0;
}

DWORD AlphaManager::HashFileName(std::string fileName)
{
	DWORD result = 0;
	MurmurHash3_x86_32(fileName.c_str(), fileName.length(), rand(), &result);
	return result;
}

std::string AlphaManager::FileNameToVFSName(std::string fileName)
{
	std::transform(fileName.begin(), fileName.end(), fileName.begin(), ::tolower);
	
	StrReplace(fileName, "\\", "/");
	StrReplace(fileName, "d_", "d:");
	return fileName;
}

void AlphaManager::Initialize(std::string packList)
{
	std::ifstream fileList(packList);
	if (!fileList.is_open()) {
		//Error: Cannot read file list!
		return;
	}
}

bool AlphaManager::LoadPack(std::string fileName)
{
	ZipArchive::Ptr archive = ZipFile::Open(fileName);
	size_t entries = archive->GetEntriesCount();
	//archive->GetComment()
	for (size_t i = 0; i < entries; ++i)
	{
		TVFSEntry entry;
		
		entry.iIndex = i;
		entry.sPackName = fileName;

		ZipArchiveEntry::Ptr aEntry = archive->GetEntry(int(i));
		if (!aEntry) {
			//Error: failed to get entry for file i
			continue;
		}

		entry.sArchivedName = aEntry->GetFullName();
		entry.sVFSName = FileNameToVFSName(entry.sArchivedName);
		
		if (!Exists(entry.sVFSName) || bAllowOverride) {
			DWORD dwNameHash = HashFileName(entry.sVFSName);
			mFiles[dwNameHash] = entry;
			vNames.push_back(entry.sVFSName);
		}
	}
	//SCompareStringByLength comparator;
	std::sort(vNames.begin(), vNames.end());//No need for comparator: default behavior sorts strings alphabetically.
	return true;
}

bool AlphaManager::AddFile(std::string filePath, std::string basePath, std::string pack, bool bCreate)
{
	std::vector<std::string> temp;
	temp.push_back(filePath);
	return AddFiles(temp, basePath, pack, bCreate);
}

bool AlphaManager::AddFiles(std::vector<std::string> filesPath, std::string basePath, std::string pack, bool bCreate)
{
	ZipArchive::Ptr archive;
	if (!FileExists(pack) && !bCreate) {
		//Error: pack does not exist
		return false;
	}
	else if (!FileExists(pack) && bCreate) {
		archive = ZipArchive::Create();
	}
	else if (FileExists(pack)) {
		archive = ZipFile::Open(pack);
	}

	int iMissingCount = 0;
	int iFailedCount = 0;
	int iOverwrittenCount = 0;
	for (std::vector<std::string>::iterator it = filesPath.begin(); it != filesPath.end(); it++) {
		std::string filePath = *it;
		if (!FileExists(filePath)) {
			//Error: input file does not exist
			iMissingCount++;
			continue;
		}

		std::ifstream contentStream(filePath, std::ios::binary);
		if (!contentStream.is_open()) {
			//Error: unable to open input file
			iFailedCount++;
			continue;
		}

		//Get path inside of archive
		std::string vfsPath = filePath;
		StrReplace(vfsPath, basePath, "");
		
		//Create and setup file entry
		ZipArchiveEntry::Ptr entry = archive->GetEntry(vfsPath);
		if (!entry) {
			entry = archive->CreateEntry(vfsPath);
		}
		else
			iOverwrittenCount++;
		
		if (!entry) {
			//Error: unable to create entry
			iFailedCount++;
			continue;
		}
		if (sPassword != "")
			entry->SetPassword(sPassword);
		entry->UseDataDescriptor();

		//Initialize compression method
		DeflateMethod::Ptr ctx = DeflateMethod::Create();
		ctx->SetCompressionLevel(DeflateMethod::CompressionLevel::Default);

		//Compress thr file with immediate mode, so we can close it.
		entry->SetCompressionStream(contentStream, ctx, ZipArchiveEntry::CompressionMode::Immediate);

		//Close the input file.
		contentStream.close();
	}
	ZipFile::SaveAndClose(archive, pack);
	return true;
}

void  AlphaManager::StrReplace(std::string& source, std::string const& find, std::string const& replace)
{
	for (std::string::size_type i = 0; (i = source.find(find, i)) != std::string::npos;)
	{
		source.replace(i, find.length(), replace);
		i += replace.length();
	}
}

bool AlphaManager::ListDiskDir(std::string baseDir, std::vector<std::string>& outList)
{
	for (auto it : RecursiveDirectoryRange(baseDir))
	{
		outList.push_back(it.path().string());
	}
	return outList.size() > 0;
}

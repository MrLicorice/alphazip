// stdafx.h : file di inclusione per file di inclusione di sistema standard
// o file di inclusione specifici del progetto utilizzati di frequente, ma
// modificati raramente
//

#pragma once

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>
#include <string>
#include <vector>
#include <map>
#include <memory>
#include <experimental\filesystem>
#include <fstream>
#define WIN32_LEAN_AND_MEAN
#include <windows.h>

// TODO: fare riferimento qui alle intestazioni aggiuntive richieste dal programma

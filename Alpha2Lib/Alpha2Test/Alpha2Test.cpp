// Alpha2Test.cpp : definisce il punto di ingresso dell'applicazione console.
//

#include "stdafx.h"
#include "../Alpha2Lib/AlphaManager.h"
#include <time.h>
#define PACKNAME "test.zip"
#pragma comment( lib, "Alpha2Lib.lib" )

int main()
{
	time_t start_time, end_cpr_time, end_time;
	time(&start_time);
	std::vector<std::string> fileList;
	/*AlphaManager am;
	if (!am.ListDiskDir("test/", fileList)) {
		printf("Disk file list is empty!");
		system("pause");
		return 0;
	}
	printf("Adding %d files.\n", fileList.size());
	am.AddFiles(fileList, "test/", PACKNAME, true);
	printf("\tComplete.\n");*/
	time(&end_cpr_time);
	
	fileList.clear();
	AlphaManager bm;
	printf("Loading pack %s\n", PACKNAME);
	bm.LoadPack(PACKNAME);
	printf("Pack contents:\n");
	if (!bm.GetVFSList(fileList)) {
		printf("Pack file list is empty!");
		system("pause");
		return 0;
	}
	for (std::vector<std::string>::iterator it = fileList.begin(); it != fileList.end(); it++) {
		std::string fi = *it;
		printf("\t%s : %s\n", PACKNAME, fi.c_str());
		std::unique_ptr<char> result;
		int resultSize = 0;
		bm.Get(fi, &result, &resultSize);
		std::string resultPath = "extracted/" + fi;
		bm.StrReplace(resultPath, "d:", "d_");
		printf("\tExtracting to: %s", resultPath.c_str()); fflush(stdout);
		std::experimental::filesystem::create_directories(std::experimental::filesystem::path(resultPath).parent_path());

		std::ofstream out(resultPath);
		if (!out.is_open()) {
			fprintf(stderr, "Error saving file %s\n", fi.c_str());
			continue;
		}
		out.write(result.get(), resultSize);
		out.close();
	}
	time(&end_time);

	printf("Compression time: %llus, ExtractionTime: %llus, TotalTime: %llus\n", end_cpr_time - start_time, end_time - end_cpr_time, end_time - start_time);
	system("pause");
}

